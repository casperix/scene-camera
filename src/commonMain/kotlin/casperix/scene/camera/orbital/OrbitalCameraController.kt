package casperix.scene.camera.orbital

import casperix.app.window.Cursor
import casperix.input.InputDispatcher
import casperix.misc.DisposableHolder
import casperix.scene.PenetrationDetector
import casperix.signals.concrete.Future
import casperix.signals.concrete.Promise

/**
 *		Camera for comfortable observe plane, or sphere object
 *		The camera position is determined by spherical coordinates plus pivot offset (pivot on XY plane)
 */
class OrbitalCameraController(
	onFrame: Future<Double>,
	onCursor:Promise<Cursor>?,
	inputDispatcher: InputDispatcher,
	penetrationDetector: PenetrationDetector? = null,
	inputSettings: OrbitalCameraInputSettings,
	transformSettings: OrbitalCameraTransformSettings,
	val outputTransform: (OrbitalCameraState) -> Unit
) : DisposableHolder() {
	val smoother = OrbitalCameraSmoother(onFrame) { outputTransform(it) }
	val collider = OrbitalCameraCollider(penetrationDetector) { smoother.inputState(it) }
	val transformer = OrbitalCameraTransformer(transformSettings) { collider.inputState(it) }
	val input = OrbitalCameraInput(onFrame, onCursor, inputDispatcher, inputSettings, transformer)

	init {
		components.add(smoother)
		components.add(input)
	}
}
