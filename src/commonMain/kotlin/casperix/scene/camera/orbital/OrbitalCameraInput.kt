package casperix.scene.camera.orbital

import casperix.app.window.Cursor
import casperix.app.window.SystemCursor
import casperix.input.*
import casperix.math.vector.float64.Vector2d
import casperix.misc.DisposableHolder
import casperix.misc.clamp
import casperix.signals.concrete.Future
import casperix.signals.concrete.Promise
import casperix.signals.then


/**
 * 	Действия пользователя преобразует в команды камере
 */
class OrbitalCameraInput(
	val onFrame: Future<Double>?,
	val onCursor: Promise<Cursor>? = null,
	val inputDispatcher: InputDispatcher,
	var settings: OrbitalCameraInputSettings,
	val transformController: OrbitalCameraTransformer
) : DisposableHolder() {

	private var pointerRotation = false
	private var pointerTranslation = false
	private var lastPosition = Vector2d.ZERO

	private var rotateVerticalIncrease = false
	private var rotateVerticalDecrease = false
	private var rotateHorizontalIncrease = false
	private var rotateHorizontalDecrease = false
	private var zoomIncrease = false
	private var zoomDecrease = false

	private var movingUp = false
	private var movingDown = false
	private var movingRight = false
	private var movingLeft = false

	init {
		inputDispatcher.apply {
			onMouseWheel.then(components, ::onMouseWheel)
			onTouchDown.then(components, ::onTouchDown)
			onTouchDragged.then(components, ::onTouchMove)
			onTouchUp.then(components, ::onTouchUp)

			onKeyDown.then(components, ::onKeyDown)
			onKeyUp.then(components, ::onKeyUp)
		}

		onFrame?.then(components) { tick ->
			update(tick)
		}

//		TODO:	apply for JS only
//		document.addEventListener("mouseout", {
//			dropMouse()
//		})
	}

	fun update(tick: Double) {
		if (movingUp || movingDown || movingLeft || movingRight) {
			val movingAlongY = if (movingDown) -1.0 else if (movingUp) 1.0 else 0.0
			val movingAlongX = if (movingLeft) -1.0 else if (movingRight) 1.0 else 0.0
			val offset = Vector2d(movingAlongX, movingAlongY) * tick * settings.keyboard.translateSpeed * getTranslateSpeedFactor()
			transformController.translate(offset.x, offset.y)
		}

		if (rotateVerticalIncrease || rotateVerticalDecrease || rotateHorizontalIncrease || rotateHorizontalDecrease) {
			val rotateVertical = if (rotateVerticalDecrease) -1.0 else if (rotateVerticalIncrease) 1.0 else 0.0
			val rotateHorizontal = if (rotateHorizontalDecrease) -1.0 else if (rotateHorizontalIncrease) 1.0 else 0.0

			transformController.rotate(rotateHorizontal * tick * settings.keyboard.rotateHorizontalSpeed, rotateVertical * tick * settings.keyboard.rotateVerticalSpeed)
		}

		if (zoomIncrease || zoomDecrease) {
			val zoom = if (zoomDecrease) -1.0 else if (zoomIncrease) 1.0 else 0.0
			transformController.zoom(zoom * settings.keyboard.zoomSpeed)
		}
	}

	private fun onKeyDown(event: KeyDown) {
		if (event.captured) return

		settings.keyboard.apply {
			if (rotateVerticalAngleIncreaseKeys.contains(event.button)) rotateVerticalIncrease = true
			if (rotateVerticalAngleDecreaseKeys.contains(event.button)) rotateVerticalDecrease = true

			if (rotateHorizontalAngleIncreaseKeys.contains(event.button)) rotateHorizontalIncrease = true
			if (rotateHorizontalAngleDecreaseKeys.contains(event.button)) rotateHorizontalDecrease = true

			if (zoomIncreaseKeys.contains(event.button)) zoomIncrease = true
			if (zoomDecreaseKeys.contains(event.button)) zoomDecrease = true

			if (translateUpKeys.contains(event.button)) movingUp = true
			if (translateDownKeys.contains(event.button)) movingDown = true
			if (translateRightKeys.contains(event.button)) movingRight = true
			if (translateLeftKeys.contains(event.button)) movingLeft = true
		}
	}

	private fun onKeyUp(event: KeyUp) {
		settings.keyboard.apply {
			if (rotateVerticalAngleIncreaseKeys.contains(event.button)) rotateVerticalIncrease = false
			if (rotateVerticalAngleDecreaseKeys.contains(event.button)) rotateVerticalDecrease = false

			if (rotateHorizontalAngleIncreaseKeys.contains(event.button)) rotateHorizontalIncrease = false
			if (rotateHorizontalAngleDecreaseKeys.contains(event.button)) rotateHorizontalDecrease = false

			if (zoomIncreaseKeys.contains(event.button)) zoomIncrease = false
			if (zoomDecreaseKeys.contains(event.button)) zoomDecrease = false

			if (translateUpKeys.contains(event.button)) movingUp = false
			if (translateDownKeys.contains(event.button)) movingDown = false
			if (translateRightKeys.contains(event.button)) movingRight = false
			if (translateLeftKeys.contains(event.button)) movingLeft = false
		}
	}

	private fun onMouseWheel(wheel: MouseWheel) {
		if (wheel.captured) return

		settings.pointer.zoomStep?.let { zoomStep ->
			val currentChange = wheel.wheel.y.clamp(-1f, 1f) * zoomStep
			transformController.zoom(currentChange)
		}
	}

	private fun dropMouse() {
//		pointerUnlock()
		pointerRotation = false
		pointerTranslation = false
	}

	private fun onTouchDown(it: TouchDown) {
		if (it.captured) return

		if (pointerRotation || pointerTranslation) return

		if (settings.pointer.rotateKeys.contains(it.button)) {
			onCursor?.set(SystemCursor.MOVE)
			lastPosition = it.position.toVector2d()
			pointerRotation = true
		}

		if (settings.pointer.translateKeys.contains(it.button)) {
			onCursor?.set(SystemCursor.MOVE)
			lastPosition = it.position.toVector2d()
			pointerTranslation = true
		}
	}


	private fun onTouchMove(it: TouchMove) {
		if (!pointerRotation && !pointerTranslation) return
		applyPointerMove(it.position.toVector2d())
	}

	private fun applyPointerMove(nextPosition: Vector2d) {
		val movement = nextPosition - lastPosition
		lastPosition = nextPosition
		if (movement == Vector2d.ZERO) return

		settings.pointer.apply {
			if (pointerRotation) {
				val yaw = -movement.x * rotateHorizontalAngleFactor * pixelFactor
				val pitch = -movement.y * rotateVerticalAngleFactor * pixelFactor
				transformController.rotate(yaw, pitch)
			}

			if (pointerTranslation) {
				val heightEffect = getTranslateSpeedFactor()
				val forward = movement.y * translateFactor * heightEffect * pixelFactor
				val right = -movement.x * translateFactor * heightEffect * pixelFactor
				transformController.translate(right, forward)
			}
		}
	}

	private fun getTranslateSpeedFactor(): Double {
		return when (settings.translateDependsOnHeight) {
			TranslateDepends.NOTHING -> 1.0
			TranslateDepends.RANGE -> transformController.getOffset().range
			TranslateDepends.HEIGHT -> transformController.getPosition().z
		}
	}

	private fun onTouchUp(it: TouchUp) {
		if (!pointerRotation && !pointerTranslation) return
		applyPointerMove(it.position.toVector2d())

		if (settings.pointer.rotateKeys.contains(it.button)) {
			onCursor?.set(SystemCursor.DEFAULT)
			pointerRotation = false
			pointerTranslation = false
		}
		if (settings.pointer.translateKeys.contains(it.button)) {
			onCursor?.set(SystemCursor.DEFAULT)
			pointerRotation = false
			pointerTranslation = false
		}
	}
}
