package casperix.scene.camera.orbital

import casperix.math.geometry.EPSILON
import casperix.math.vector.float64.Vector3d
import casperix.math.vector.toSpherical
import casperix.scene.PenetrationDetector

class OrbitalCameraCollider(var penetrationDetector: PenetrationDetector?, val outputState: (OrbitalCameraState) -> Unit) {

	private fun resolveCollision(cameraPosition: Vector3d): Vector3d {
		val penetrationDetectorCurrent = penetrationDetector
		if (penetrationDetectorCurrent != null) {
			val penetration = penetrationDetectorCurrent(cameraPosition)
			if (penetration != null) {
				return cameraPosition - penetration * (1.0 + EPSILON)
			}
		}
		return cameraPosition
	}

	fun inputState(state: OrbitalCameraState) {
		state.apply {
			val cameraPosition = pivot.expand(0.0) + offset.fromSpherical()
			val cameraPositionFinal = resolveCollision(cameraPosition)
			val cameraOffset = (cameraPositionFinal - pivot.expand(0.0)).toSpherical()
			outputState(copy(offset = cameraOffset))
		}
	}
}
