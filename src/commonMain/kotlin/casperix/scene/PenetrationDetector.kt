package casperix.scene

import casperix.math.geometry.Line3d
import casperix.math.vector.float64.Vector3d


/**
 * 	Детектор проникновения "исходного тела"  в другие "тела"
 * 	Задается перемещение "исходного тела"
 * 	Возвращается вектор проникновения "исходного тело" в другие тела в момент завершения перемещения
 */
typealias ContinuousPenetrationDetector = (Line3d) -> Vector3d?

/**
 * 	Детектор проникновения "исходного тела"  в другие "тела"
 * 	Задается положение "исходного тела"
 * 	Возвращается вектор проникновения "исходного тело" в другие тела в момент завершения перемещения
 */
typealias PenetrationDetector = (Vector3d) -> Vector3d?